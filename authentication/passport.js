const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');

const User = require('../models/User');


// numero de rondas que hace crypt para ocultar la pass
const bcryptRound = 10;


//LocalStrategy es la estrategia de registro usando email y pass
passport.use(
  'register',
  new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true,
    },
    async (req, email, password, done) => {
      try {
        //buscamos si el usiario esta logado
        //si lo esta, ivocamos al done y no hacemos nada
        const userLoged = await User.findOne({ email: email });
        if (userLoged) {
          const error = new Error('Ha este tip@ ya lo tenemos');
          return done(error);
        }

        // tras comprobar que el usuario no existe
        //ocultamos la pass con bcrypt y el numero de rondas
        const hiddPw = await bcrypt.hash(password, bcryptRound);

        // Creamos el nuevo user con el modelo requerido de models
        const newUser = new User({
          email: email,
          //email: req.body.email,
          password: hiddPw,
          role: req.body.role,
        });
        // newUser.save agrega el nuevo usuario a la DB
        //lo guardamos en userOK parq devolverlo
        const userOK = await newUser.save();
        
        // en done se nula el error y devolvemos el usuario creado
        done(null, userOK);
      } catch (error) {
        // Si hay un error, resolvemos el callback con el error
        return done(error);
      }
    }
  )
);

//Estrategia de login, como la de registro
passport.use(
  'login',
  new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true,
    },
    async (req, email, password, done) => {
      try {
        // Primero buscamos si el usuario existe en nuestra DB
        //nos guardamos en currentUser el user que se quiere logar
        const currentUser = await User.findOne({ email: email });

        // Cuando el usuario no esta registrado en nuestra DB
        if (!currentUser) {
          const error = new Error('Ni de coña, no estas registrado');
          return done(error);
        }

        // cuando el usuario esta registrado en nuestra DB
        //comprobar si su password enviado coincide con el registrado
        //Este metodo de BCRYPT compara la pass del usuario que se quier
        //logar con la que hay guardada y securizada en la DB
        const isValidPassword = await bcrypt.compare(
          password,
          currentUser.password
        );

        // Si el password no es correcto, enviamos un error a nuestro usuario
        if (!isValidPassword) {
          const error = new Error(
            '  Upss, email & password no son correctos'
          );
          return done(error);
        }

        // Si todo se valida correctamente, eliminamos la contraseña del usuario devuelto 
        // por la db y completamos el callback con el usuario
        currentUser.password = null;
        return done(null, currentUser);
      } catch (error) {
        // Si hay un error, resolvemos el callback con el error
        return done(error);
      }
    }
  )
);




// Esta función usará el usuario de req.LogIn para registrar su id.
passport.serializeUser((user, done) => {
  return done(null, user._id);
});

// Esta función buscará un usuario dada su _id en la DB y populará req.user si existe
passport.deserializeUser(async (userId, done) => {
  try {
    const existingUser = await User.findById(userId);
    return done(null, existingUser);
  } catch (err) {
    return done(err);
  }
});
