
//requerimos express
const express = require('express');
//requerimos path
//para que funciona el midelware que espone las imagenes.
const path = require('path');
//para gestionar variables de entorno
//asi se carga la configuracion de entornos
// antes que nada en nuestro proyecto
require('dotenv').config();

//AUTENTICACION
//Requerimos expres.sesion
const session = require('express-session');
//requerimos passport en el index y nuestro archivo de autenticacion
const passport = require('passport');
require('./authentication/passport'); 


//UTILS
//requerimos log.js para registar errores
const logError = require('./utils/log');
// Requerimos el archivo de configuración de nuestra DB
//que realiza la conexion con nuestra database node_s3
const connect = require('./utils/db');


const softwareRoutes = require('./routes/software.routes');
const userRoutes = require('./routes/user.routes');
const employeeRoutes = require('./routes/employee.routes');



//CONFIGURACION DEL SERVIDOR
connect(); //CONECTA CON LA DB 
const server = express();
const PORT = 3000;

// Middlewares, para que comvierta lo que llega
//en el body a json y se entere.
server.use(express.json());
server.use(express.urlencoded({ extended: true }));

//configuramos la sesion.
server.use(
  session({
    secret: 'upgradehub_node', // ¡Este secreto tendremos que cambiarlo en producción!
    resave: false, // Solo guardará la sesión si hay cambios en ella.
    saveUninitialized: false, // Lo usaremos como false debido a que gestionamos nuestra sesión con Passport
    cookie: {
      maxAge: 3600000 // Milisegundos de duración de nuestra cookie, en este caso será una hora.
    },
  })
);
//middleware que inicializa passport
server.use(passport.initialize())
//despues de instalar express-sesion
//Esto añadirá sesiones a nuestros usuarios
server.use(passport.session()); 

//para que se exponga lo que hay en public 
//y asi poder ver las imagenes desde el frontal.
server.use(express.static(path.join(__dirname, 'public')));

//ROUTES
// una ruta para cada entidad de la base de datos
server.use('/software', softwareRoutes);
server.use('/user', userRoutes);
server.use('/employee', employeeRoutes);

//CONTOL DE ERRORES
//si metemos una ruta que no existe nos da un error
//aqui empezamos a controlar los errores.
server.use('*', (req, res, next) => {
  const msg = 'Route not found'
  const error = new Error('Route not found');
  
  error.status = 404;
  next(error); // Lanzamos la función next() con un error
  const log = `${msg}
  ${req.path}
  ${new Date().toISOString()}\n`;
  logError(log) ; //funcion que registra el error en el archivo con el log que hemos creado
  
});




server.use((error, req, res, next) => {
  return res.status(error.status || 500).json(error.message || 'Unexpected error');
});





//EL SEVIDOR SE LEVANTA EN LOCALHOST PUERTO 3000
server.listen(PORT, () => {
    console.log(`Server running in http://localhost:${PORT}`);
  });