const express = require('express');
const Employee = require('../models/employee');
//const upload = require('../midelwares/file.midelware')
const fileMiddleware = require('../midelwares/file.midelware')

const router = express.Router();

//DEVUELVE LISTADO DE SOFWARE COMPLETO
router.get("/", async (req, res, next) => {
    try {
      const employee = await Employee.find().populate('software');  
      //const employee = await Employee.find();
      return res.status(200).json(employee);
    } catch (err) {
      return next(err);
    }
  });

/*******/
/*******/
//CREACION DE EMPLOYEE 
//CON MULTER
/*router.post("/", [upload.single('picture')], async (req, res, next) => {
    try {
      //si hay req.file guarda el nombre en employeePicture
      //para añadirlo al empleado que creamos.
      const employeePicture = req.file ? req.file.filename : null;
      const newEmployee = new Employee({
        email: req.body.email,
        name: req.body.name,
        computer: req.body.computer,
        software: [],
        picture: employeePicture
      });
      const createdEmployee = await newEmployee.save();
      return res.status(201).json(createdEmployee);
      //el 201 es el de creacion, mas especifico
    } catch (err) {
      return next(err);
    }
});*/
/*******/
/*******/
//CON CLOUDINARY
            
router.post("/", [fileMiddleware.upload.single('picture'), fileMiddleware.uploadToCloudinary], async (req, res, next) => {
    try {

      //si hay req.file guarda el nombre en employeePicture
      //para añadirlo al empleado que creamos.
      const cloudinaryUrl = req.file_url ? req.file_url : null;
      
      
      const newEmployee = new Employee({
        email: req.body.email,
        name: req.body.name,
        computer: req.body.computer,
        software: [],
        picture: cloudinaryUrl
      });
      const createdEmployee = await newEmployee.save();
      return res.status(201).json(createdEmployee);
      //el 201 es el de creacion, mas especifico
    } catch (err) {
      return next(err);
    }
});

/*******/
/*******/
//UPDATE de un empledado para añadirle un software instalado
router.put('/add-software', async (req, res, next) => {
  if (req.user) {
    if (req.user.role === "admin") {
      try {
        const { idEmployee, idSoftware } = req.body;
        //buscamos el idEmployee del empledado y el campo de sofware
        //le metemos el idSoftware del software que tien instalado
        //insertemos el id en el array de datos del objeto editado
        const updateEmployee = await Employee.findByIdAndUpdate(
            idEmployee,
            { $push: { software: idSoftware } },
            { new: true }
        );
        return res.status(200).json(updateEmployee);
    } catch (err) {
        return next(err);
    }
    } else {
      return res.status(200).json("Upss, No eres Admin");
    }
  } else {
    return res.status(200).json("ohhhh no estas logado");
  }
    
});
/*******/
/*******/
//DELETE  recogenos el id, buscar y eliminar ese id
router.delete('/:id', async (req, res, next) => {
  if (req.user) {
    if (req.user.role === "admin") {
      try {
        const {id} = req.params;
        //const deleteSoftware = await Software.findByIdAndDelete(id);
        await Employee.findByIdAndDelete(id);
        //no necesrio const deleteSoftware puesto que no lo devolvemos 
        //devolvemos solo mensaje de exito
        return res.status(200).json('hasta luegoooooor')
    } catch (err) {
        return next(err);
    }
    } else {
      return res.status(200).json("Upss, No eres Admin");
    }
  } else {
    return res.status(200).json("ohhhh no estas logado");
  }
    
});



module.exports = router;