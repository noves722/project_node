const express = require("express");
const passport = require("passport");
const mongoose = require("mongoose");

//const Employee = require('../models/user');

const router = express.Router();

//REGISTRO DE USER
router.post("/register", (req, res, next) => {
  // Invocamos a la autenticación de Passport

  const done = (error, user) => {
    // Si hay un error, llamamos a nuestro controlador de errores
    if (error) {
      return next(error);
    }
    //nulamos la pass para que no viaje de vuelta.
    user.password = null;
    //login es un metodo de passport
    //para permitir logarnos
    req.logIn(user, (error) => {
      // Si hay un error logeando al usuario, resolvemos el controlador
      if (error) {
        return next(error);
      }
      // Si no hay error, devolvemos al usuario logueado
      return res.status(201).json(user);
    });
  };
  //passport.authenticate nos dice su el usuaria se puede logar
  //y lo hemos regustado
  const register = passport.authenticate("register", done);
  //Ahora invocamos register con la req para registrar al usuario
  //obtenido del post
  register(req);
});


//LOGIN DE USER
router.post("/login", (req, res, next) => {
  const done = (error, user) => {
    if (error) {
      return next(error);
    }
    user.password = null;
    req.logIn(user, (error) => {
      // Si hay un error logeando al usuario, resolvemos el controlador
      if (error) {
        return next(error);
      }
      // Si no hay error, devolvemos al usuario logueado
      return res.status(200).json(user);
    });
  };

  const login = passport.authenticate("login", done);
  login(req);
});

//LOGOUT DE USER
router.post("/logout", (req, res, next) => {
  req.logout((err) => {
    if (err) {
      return next(err);
    }
    return res.status(200).send('Hata luego... Mari Carmen');
  });
});

module.exports = router;
