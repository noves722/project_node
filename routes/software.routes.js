const express = require("express");
const mongoose = require("mongoose");
const Software = require("../models/software");
const Employee = require("../models/employee")

const router = express.Router();

//DEVUELVE LISTADO DE SOFWARE COMPLETO
router.get("/", async (req, res, next) => {
  
  try {
    const software = await Software.find();
    return res.status(200).json(software);
  } catch (err) {
    return next(err);
  }
});
//DEVUELVE SOFWARE CORRESPONDIENTE A UN ID
router.get("/id/:id", async (req, res, next) => {
  const idSoft = req.params.id;
  try {
    const software = await Software.findById(idSoft);
    return res.status(200).json(software);
  } catch (err) {
    return next(err);
  }
});
//DEVUELVE SOFWARE CON EL NOMBRE INDICADO
router.get("/name/:name", async (req, res, next) => {
  const nameSoft = req.params.name;
  try {
    const software = await Software.find({ name: nameSoft });
    return res.status(200).json(software);
  } catch (err) {
    return next(err);
  }
});
//DEVUELVE SOFWARE DEL TIPO QUE SE QUIERA
router.get("/type/:type", async (req, res, next) => {
  const typeSoft = req.params.type;
  try {
    const software = await Software.find({ types: { $in: [typeSoft] } });
    return res.status(200).json(software);
  } catch (err) {
    return next(err);
  }
});
//DEVUELVE SOFWARE CON LICENCIA LIBRE O EN VIGOR DESPUES
//DE LA FECHA INDICADA.
router.get("/date/:year/:month/:day", async (req, res, next) => {
  const yearSoft = req.params.year;
  const monthSoft = req.params.month;
  const daySoft = req.params.day;
  const dateSoft = new Date(yearSoft, monthSoft, daySoft);

  console.log(dateSoft);
  console.log(yearSoft);
  console.log(monthSoft);
  console.log(daySoft);

  try {
    //const software = await Software.find({license: ""})
    const software = await Software.find({ license: { $gte: dateSoft } });
    return res.status(200).json(software);
  } catch (err) {
    return next(err);
  }
});

//CREACION DE SOFTWARE recoge los datos de req.body
//creamos newSofware con los dartos y lanzamos el .save()
//que lo añade a la DB
router.post("/", async (req, res, next) => {
  if (req.user && req.user.role === "admin") {
    try {
      //console.log(req.body);
      const newSoftware = new Software({
        name: req.body.name,
        free: req.body.free,
        types: req.body.types,
        license: req.body.license,
      });
      const createdSoftware = await newSoftware.save();
      return res.status(201).json(createdSoftware);
      //el 201 es el de creacion, mas especifico
    } catch (err) {
      return next(err);
    }
  } else {
    return res.send("PIRATE.. no eres administrador");
  }
});

//UPDATE de un software por id
router.put("/:id", async (req, res, next) => {
  if (req.user) {
    if (req.user.role === "admin") {
      try {
        const { id } = req.params; //Recuperamos el id de la url
        const softwareModify = new Software(req.body); //se crea nuevo soft con la información del body
        softwareModify._id = id; //añadimos la propiedad _id al nuevo software
        const softwareUpdated = await Software.findByIdAndUpdate(
          id,
          softwareModify
        );
        if (softwareUpdated) {
          return res.status(200).json(softwareModify); //devolvemos el modificado
        } else {
          return res.status(200).json("Te columpiaste, el ID no esta en la DB");
        }
      } catch (err) {
        return next(err);
      }
    } else {
      return res.status(200).json("Upss, No eres Admin");
    }
  } else {
    return res.status(200).json("ohhhh no estas logado");
  }
  
});

//delete de un software por id
router.delete("/delete/:id", async (req, res, next) => {
  if (req.user) {
    if (req.user.role === "admin") {
      try {
        const { id } = req.params;

       /* const updatesoft = await Employee.updateOne(id, {
          $pullAll: {
              software: [{_id: id}],
          },
      });*/

        
        
        //const software = await Software.find({ types: { $in: [typeSoft] } });
        //const deleteSoftware = await Software.findByIdAndDelete(id);
        await Software.findByIdAndDelete(id);
        //no necesrio const deleteSoftware puesto que no lo devolvemos
        //devolvemos solo mensaje de exito
        return res.status(200).json("A la mierda el soft");
      } catch (err) {
        return next(err);
      }
    } else {
      return res.status(200).json("Upss, No eres Admin");
    }
  } else {
    return res.status(200).json("ohhhh no estas logado");
  }
});

module.exports = router;


/*
if (req.user) {
    if (req.user.role === "admin") {
      
    } else {
      return res.status(200).json("Upss, No eres Admin");
    }
  } else {
    return res.status(200).json("ohhhh no estas logado");
  }
*/ 
