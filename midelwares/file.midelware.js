const multer = require("multer");
const path = require("path");

//NECESARIO PARA CLOUDINARY
// Importaremos las librerías necesarias para la nueva función
const fs = require("fs");
const cloudinary = require("cloudinary").v2;

//diskStorage necesita el nombre y el archivo a subir
const storage = multer.diskStorage({
  //nombre del archivo, le metemos la fecha y el nombre.
  filename: (req, file, cb) => {
    cb(null, `${Date.now()}-${file.originalname}`);
  },
  //destino es la carpeta public que hemos creado.
  destination: (req, file, cb) => {
    cb(null, path.join(__dirname, "../public"));
  },
});

//tipos de archivos que debe estar pendiente de subir
const VALID_FILE_TYPES = ["image/png", "image/jpg", "image/jpeg"];
//si el archivo no esta entre los tipos, error 'Invalid file type'
const fileFilter = (req, file, cb) => {
  if (!VALID_FILE_TYPES.includes(file.mimetype)) {
    cb(new Error("Invalid file type"));
  } else {
    cb(null, true);
  }
};

const upload = multer({
  storage,
  fileFilter,
});

//CLOUDINARY
// Ahora tenemos un nuevo middleware de subida de archivos
const uploadToCloudinary = async (req, res, next) => {
  if (req.file) {
    try {
      const filePath = req.file.path;
      const image = await cloudinary.uploader.upload(filePath);

      // Borramos el archivo local
      await fs.unlinkSync(filePath);

      // Añadimos la propiedad file_url a nuestro Request
      req.file_url = image.secure_url;
      return next();
    } catch (error) {
      return next(error);
    }
  } else {
    return next();
  }
};

module.exports = { upload: upload, uploadToCloudinary };

//module.exports = upload ;
