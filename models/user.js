const mongoose = require('mongoose');


const Schema = mongoose.Schema;

// Creamos el esquema de personajes
const userSchema = new Schema(
    {    
        email: {type: String, required:true, lowercase: true},
        password: {type: String, require:true},
        role: {type: String, required:true}      
        
      },
  {
    // Esta propiedad servirá para guardar las fechas de creación y actualización de los documentos
    timestamps: true,
  }
);

// Creamos y exportamos el modelo Character
const User = mongoose.model('User', userSchema);
module.exports = User;