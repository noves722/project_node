const mongoose = require('mongoose');


const Schema = mongoose.Schema;

// Creamos el esquema de personajes
const softwareSchema = new Schema(
    {    
        name: {type: String, required:true, lowercase: true},
        free: {type: Boolean},
        types: {type: String},       
        license: {type: Date},
        //picture: { type: String }
      },
  {
    // Esta propiedad servirá para guardar las fechas de creación y actualización de los documentos
    timestamps: true,
  }
);

// Creamos y exportamos el modelo Character
const Software = mongoose.model('Software', softwareSchema);
module.exports = Software;