const mongoose = require('mongoose');


const Schema = mongoose.Schema;

// Creamos el esquema de personajes
const employeeSchema = new Schema(
    {    
        email: {type: String, required:true, lowercase: true},
        name: {type: String, required:true, lowercase: true},
        computer: {type: String},
        // Tipo mongoose Id y referencia al modelo Software
        software: [{ type: mongoose.Types.ObjectId, ref: 'Software' }],
        picture: { type: String }
           
      },
  {
    // Esta propiedad servirá para guardar las fechas de creación y actualización de los documentos
    timestamps: true,
  }
);

// Creamos y exportamos el modelo Character
const Employee = mongoose.model('Employee', employeeSchema);
module.exports = Employee;