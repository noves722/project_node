const mongoose = require("mongoose");

const Software = require("./models/Software");
const Employee = require("./models/employee");
//const User = require("./models/User");
console.log('arreglado DB');

const software = [
  {
    name: "word",
    free: false,
    types: "ofimatic",
    license: new Date(2022,11,20),
  },
  {
    name: "excel",
    free: false,
    types: "ofimatic",
    license: new Date(2022,11,22),
  },
  {
    name: "libreoffice",
    free: true,
    types: "ofimatic",
    license: "",
  },
  {
    name: "photoshop",
    free: false,
    types: "edition",
    license: new Date(2023,11,31),
  },
  {
    name: "gimp",
    free: true,
    types: "edition",
    license: "",
  },
  {
    name: "VS code",
    free: true,
    types: "programming",
    license: "",
  },
];

const employee = [
  {
    email: "jonoves@empresa.com",
    name: "Jose Maria Noves",
    computer:"pestol-001",
    software: [
      "6395dddae70e7183a951c2c5",
      "6395dddae70e7183a951c2c6",
      "6395dddae70e7183a951c2c7",
      "6395dddae70e7183a951c2c8",
      "6395dddae70e7183a951c2c9",
      "6395dddae70e7183a951c2ca"
    ],
  },
  {
    email: "jufern@empresa.com",
    name: "Juan Fernadez",
    computer:"pestol-002",
    software: [
      "6395dddae70e7183a951c2ca"
    ],
  },
  {
    email: "ancamp@empresa.com",
    name: "Ana Campos",
    computer:"pestol-003",
    software: [
      "6395dddae70e7183a951c2c8"
    ],
  }
  
];

// Completa el código usando mongoose y el array para guardar la seed en nuestra DB
const softwareDocuments = software.map((software) => new Software(software));
//const employeeDocuments = employee.map((employee) => new Employee(employee));
//const userDocuments = user.map((user) => new User(user));
// conectamos con la DB y nos desconectamos tras insertar documentos
mongoose
  .connect("mongodb://localhost:27017/node_project", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  /*********
   * SEMBRAR COLECCION DE SOFWARE
   *********/
  .then(async () => {
    // Utilizando Software.find() obtendremos un array con todos los personajes de la db
    const allSoftware = await Software.find();

    // Si existen software en la BD previamente, dropearemos la colección
    if (allSoftware.length) {
      await Software.collection.drop(); //La función drop borra la colección
    }
  })
  .catch((err) => console.log(`Error deleting data: ${err}`))
  .then(async () => {
    // Una vez vaciada la db de los personajes, usaremos el array softwareDocuments
    // para llenar nuestra base de datos con todo el softwar.
    await Software.insertMany(softwareDocuments);
  })
  .catch((err) => console.log(`Error creating data: ${err}`))
  /*********
   * SEMBRAR COLECCION EMPLOYEES
   ********/
  /*.then(async () => {
    
    const allUser = await User.find();

    if (allUser.length) {
      await User.collection.drop(); 
    }
  })
  .catch((err) => console.log(`Error deleting data: ${err}`))
  .then(async () => {
    
    await User.insertMany(userDocuments);
  })
  .catch((err) => console.log(`Error creating data: ${err}`))*/


/*********
   * SEMBRAR COLECCION EMPLOYEES
   ********/
 /*.then(async () => {
    
  const allEmployees = await Employee.find();

  if (allEmployees.length) {
    await Employee.collection.drop(); 
  }
})
.catch((err) => console.log(`Error deleting data: ${err}`))
.then(async () => {
  
  await Employee.insertMany(employeeDocuments);
})
.catch((err) => console.log(`Error creating data: ${err}`))*/


  // Por último nos desconectaremos de la DB.
  .finally(() => mongoose.disconnect());
